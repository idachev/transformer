Expression (1):

generateEmail[
	withNameAs{
		firstName: useAll,
		lastName: useAll,
		concatenateWith: _
	};
	atDomainAs{
		entity: useAll,
		domain: useAll
	}
]

Output: ivo_georgiev@peoplespheres.com

Base 64 Encoded:

Z2VuZXJhdGVFbWFpbFsNCgl3aXRoTmFtZUFzew0KCQlmaXJzdE5hbWU6IHVzZUFsbCwNCgkJbGFzdE5hbWU6IHVzZUFsbCwNCgkJY29uY2F0ZW5hdGVXaXRoOiBfDQoJfTsNCglhdERvbWFpbkFzew0KCQllbnRpdHk6IHVzZUFsbCwNCgkJZG9tYWluOiB1c2VBbGwNCgl9DQpd

Expression (2):

generateEmail[
	withNameAs{
		fullName: useAll,
		concatenateWith: _
	};
	atDomainAs{
		entity: useAll,
		domain: useAll
	}
]

Output: ivo_ivanov_georgiev@peoplespheres.com

Base 64 Encoded:

Z2VuZXJhdGVFbWFpbFsNCgl3aXRoTmFtZUFzew0KCQlmdWxsTmFtZTogdXNlQWxsLA0KCQljb25jYXRlbmF0ZVdpdGg6IF8NCgl9Ow0KCWF0RG9tYWluQXN7DQoJCWVudGl0eTogdXNlQWxsLA0KCQlkb21haW46IHVzZUFsbA0KCX0NCl0=

Expression (3):

generateEmail[
	withNameAs{
		firstName: useAll,
		role: useAll,
		concatenateWith: .
	};
	atDomainAs{
	    resourceType: useAll,
		entity: useAll,
		domain: useAll
	}
]

Output: ivo.dev@internal.peoplespheres.com

Base 64 Encoded:

Z2VuZXJhdGVFbWFpbFsNCgl3aXRoTmFtZUFzew0KCQlmaXJzdE5hbWU6IHVzZUFsbCwNCgkJcm9sZTogdXNlQWxsLA0KCQljb25jYXRlbmF0ZVdpdGg6IC4NCgl9Ow0KCWF0RG9tYWluQXN7DQoJICAgIHJlc291cmNlVHlwZTogdXNlQWxsLA0KCQllbnRpdHk6IHVzZUFsbCwNCgkJZG9tYWluOiB1c2VBbGwNCgl9DQpd

Expression (4):

generateEmail[
	withNameAs{
		firstName: useFirst(1),
		middleName: useFirst(4),
		lastName: useLast(1),
		department: useAll,
		concatenateWith: -
	};
	atDomainAs{
		entityDomain: useAll
	}
]

Output: i-ivan-v-backend@peoplespheres.com

Base 64 Encoded:

Z2VuZXJhdGVFbWFpbFsNCgl3aXRoTmFtZUFzew0KCQlmaXJzdE5hbWU6IHVzZUZpcnN0KDEpLA0KCQltaWRkbGVOYW1lOiB1c2VGaXJzdCg0KSwNCgkJbGFzdE5hbWU6IHVzZUxhc3QoMSksDQoJCWRlcGFydG1lbnQ6IHVzZUFsbCwNCgkJY29uY2F0ZW5hdGVXaXRoOiAtDQoJfTsNCglhdERvbWFpbkFzew0KCQllbnRpdHlEb21haW46IHVzZUFsbA0KCX0NCl0=

Expression (5):

generateEmail[
	withNameAs{
		lastName: useAll,
		department: useAll,
		role: useAll,
		concatenateWith: .
	};
	atDomainAs{
	    resourceType: useAll,
		entityDomain: useAll,
	}
]

Output: georgiev.backend.dev@internal.peoplespheres.com

Base 64 Encoded:

Z2VuZXJhdGVFbWFpbFsNCgl3aXRoTmFtZUFzew0KCQlsYXN0TmFtZTogdXNlQWxsLA0KCQlkZXBhcnRtZW50OiB1c2VBbGwsDQoJCXJvbGU6IHVzZUFsbCwNCgkJY29uY2F0ZW5hdGVXaXRoOiAuDQoJfTsNCglhdERvbWFpbkFzew0KCSAgICByZXNvdXJjZVR5cGU6IHVzZUFsbCwNCgkJZW50aXR5RG9tYWluOiB1c2VBbGwsDQoJfQ0KXQ==

Expression (6):

generateEmail[
	withNameAs{
		fullName: useFirst(1),
		role: useAll,
		department: useAll,
		concatenateWith: .
	};
	atDomainAs{
	    resourceType: useAll,
		entity: useAll,
		domain: useAll
	}
]

Output: i.i.g.dev.backend@internal.peoplespheres.com

Base 64 Encoded:

Z2VuZXJhdGVFbWFpbFsNCgl3aXRoTmFtZUFzew0KCQlmdWxsTmFtZTogdXNlRmlyc3QoMSksDQoJCXJvbGU6IHVzZUFsbCwNCgkJZGVwYXJ0bWVudDogdXNlQWxsLA0KCQljb25jYXRlbmF0ZVdpdGg6IC4NCgl9Ow0KCWF0RG9tYWluQXN7DQoJICAgIHJlc291cmNlVHlwZTogdXNlQWxsLA0KCQllbnRpdHk6IHVzZUFsbCwNCgkJZG9tYWluOiB1c2VBbGwNCgl9DQpd
