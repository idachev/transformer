package com.peoplespheres.transformer.modulith;

import com.peoplespheres.transformer.TransformerApplication;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.modulith.core.ApplicationModules;

class ModularityTest {

	private final ApplicationModules modules = ApplicationModules.of(TransformerApplication.class);

	@Test
	@DisplayName("Application does not violate Modulith rules")
	void verifiesModularStructure() {
		modules.verify();
	}

}