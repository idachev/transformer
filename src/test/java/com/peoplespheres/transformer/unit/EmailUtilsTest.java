package com.peoplespheres.transformer.unit;

import com.peoplespheres.transformer.generator.internal.validation.TransformerValidator;
import com.peoplespheres.transformer.util.EmailUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

class EmailUtilsTest {

	private final TransformerValidator validator = new TransformerValidator();


	@ParameterizedTest(name = "[{index}] Successfully filters out invalid email: {0}")
	@MethodSource("provideInvalidEmailsAndExpectedOutput")
	void givenInvalidEmailsShouldSuccessfullyFilterAsValidEmail(String inputEmail, String expected) {
		final var filtered = EmailUtils.filterIllegalCharacters(inputEmail);
		Assertions.assertEquals(expected, filtered);
		assertDoesNotThrow(() -> validator.validateEmail(filtered),
				String.format("The validation method should not throw an exception for the email: %s", filtered));
	}

	static Stream<Arguments> provideInvalidEmailsAndExpectedOutput() {
		return Stream.of(
				Arguments.of("iv&o.georgi#ev@peoplespheres.com", "ivo.georgiev@peoplespheres.com"),
				Arguments.of("ivo.georgi%ev@peoplespheres.com", "ivo.georgiev@peoplespheres.com"),
				Arguments.of("i.georgi$ev@peoplespheres.com", "i.georgiev@peoplespheres.com"),
				Arguments.of("i.georgi&ev@peoplespheres.com", "i.georgiev@peoplespheres.com"),
				Arguments.of("georgi*ev@peoplespheres.com", "georgiev@peoplespheres.com"),
				Arguments.of("georgi~!ev@peoplespheres.com", "georgi~ev@peoplespheres.com"),
				Arguments.of("ivo@peoplesphe^re.scom", "ivo@peoplesphere.scom"),
				Arguments.of("ivo@peoplesph~ere.scom", "ivo@peoplesphere.scom"),
				Arguments.of("i.georgie^^^v_hr@internal.peoplespheres.com", "i.georgiev_hr@internal.peoplespheres.com"),
				Arguments.of("i.georgi&*()ev_hr@internal.peoplespheres.com", "i.georgiev_hr@internal.peoplespheres.com"),
				Arguments.of("iv~o.georgiev@peoplesp%%heres.com", "iv~o.georgiev@peoplespheres.com"),
				Arguments.of("ivo*ge~orgi#ev@peoplespheres.com", "ivoge~orgiev@peoplespheres.com"),
				Arguments.of("i.ge&orgi$ev@peoplespheres.com", "i.georgiev@peoplespheres.com"),
				Arguments.of("i#georgi&ev@peoplespheres.com", "igeorgiev@peoplespheres.com"),
				Arguments.of("geo*#rgi*ev@peoplespheres.com", "georgiev@peoplespheres.com"),
				Arguments.of("geo~^^rgi~!ev@peoplespheres.com", "geo~rgi~ev@peoplespheres.com"),
				Arguments.of("i^v^o@peoplesphe^res.com", "ivo@peoplespheres.com"),
				Arguments.of("i~v~o@peoplesph~eres.com", "i~v~o@peoplespheres.com")
		);
	}
}
