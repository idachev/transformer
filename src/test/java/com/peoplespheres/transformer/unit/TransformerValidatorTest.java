package com.peoplespheres.transformer.unit;

import com.peoplespheres.transformer.exception.ValidationException;
import com.peoplespheres.transformer.generator.internal.validation.TransformerValidator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

class TransformerValidatorTest {

	private final TransformerValidator subject = new TransformerValidator();

	@ParameterizedTest(name = "Passes validation for email: {0}")
	@MethodSource("generateValidEmails")
	void givenValidEmailShouldPassValidation(final String validEmail) {
		assertDoesNotThrow(() -> subject.validateEmail(validEmail),
				String.format("The validation method should not throw an exception for the email: %s", validEmail));
	}

	@ParameterizedTest(name = "Throws exception for invalid email: {0}")
	@MethodSource("generateInvalidEmails")
	void givenInvalidEmailShouldThrowException(final String invalidEmail) {
		assertThatThrownBy(() -> subject.validateEmail(invalidEmail)).isInstanceOf(ValidationException.class);
	}

	private static Stream<String> generateValidEmails() {
		return Stream.of(
				"ivo.georgiev@external.peoplespheres.com",
				"ivo.georgiev@internal.peoplespheres.com",
				"ivo.georgiev.dev@external.peoplespheres.com",
				"ivo.georgiev.dev@internal.peoplespheres.com",
				"ivo.georgiev.dev.development@internal.peoplespheres.com",
				"dev.ivo.georgiev@external.peoplespheres.com",
				"dev.ivo.georgiev@internal.peoplespheres.com",
				"hr.ivo.georgiev@external.peoplespheres.com",
				"hr.ivo.georgiev@internal.peoplespheres.com",
				"i.georgiev_hr@internal.peoplespheres.com"
		);
	}

	private static Stream<String> generateInvalidEmails() {
		return Stream.of(
				"@external.peoplespheres.com",
				"ivo.georgiev@",
				"ivo.georgiev.dev.@external",
				"ivo.georgiev.dev@.",
				"ivo.georgiev.dev.development@@internal.peoplespheres.com",
				"dev.ivo.georgiev.extern@l.peoplespheres.com@",
				"ivo.georgiev@internal..peoplespheres.com",
				"hr.ivo.georgiev@.external.peoplespheres..com",
				"i.georgiev_hr@internal.peoplespheres.com."
		);
	}
}

