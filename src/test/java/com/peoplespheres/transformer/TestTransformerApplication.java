package com.peoplespheres.transformer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.TestConfiguration;

@TestConfiguration(proxyBeanMethods = false)
public class TestTransformerApplication {

	public static void main(String[] args) {
		SpringApplication.from(TransformerApplication::main).with(TestTransformerApplication.class).run(args);
	}

}
