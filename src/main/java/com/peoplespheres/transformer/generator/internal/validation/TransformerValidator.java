package com.peoplespheres.transformer.generator.internal.validation;

import com.peoplespheres.transformer.exception.ValidationException;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

import static com.peoplespheres.transformer.util.EncryptionUtils.obfuscate;
import static com.peoplespheres.transformer.util.ExceptionUtils.logAndRaise;

@Component
@RequiredArgsConstructor
public class TransformerValidator {

	private static final Logger logger = LoggerFactory.getLogger(TransformerValidator.class);
	private static final Pattern EMAIL_PATTERN = Pattern.compile("^[\\w~.-]+@([\\w~-]+\\.)+[\\w~-]{2,4}$");

	public void validateEmail(final String email) {
		final var matcher = EMAIL_PATTERN.matcher(email);
		if (!matcher.matches()) {
			throw logAndRaise(
					logger,
					String.format("Email '%s' is not valid", obfuscate(email)),
					ValidationException.class
			);
		}

		if (logger.isInfoEnabled()) {
			logger.info("Successfully validated email for user: {}", obfuscate(email));
		}
	}

}
