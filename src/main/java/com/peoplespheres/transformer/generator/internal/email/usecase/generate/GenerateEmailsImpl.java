package com.peoplespheres.transformer.generator.internal.email.usecase.generate;

import com.peoplespheres.transformer.expression.ComponentElement;
import com.peoplespheres.transformer.expression.EmailExpression;
import com.peoplespheres.transformer.expression.ExpressionUtils;
import com.peoplespheres.transformer.generator.external.api.GenerateEmails;
import com.peoplespheres.transformer.generator.internal.validation.TransformerValidator;
import com.peoplespheres.transformer.model.Email;
import com.peoplespheres.transformer.model.Emails;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.peoplespheres.transformer.expression.ExpressionUtils.extractArgumentsFrom;
import static com.peoplespheres.transformer.expression.ExpressionUtils.getNameConcatenation;
import static com.peoplespheres.transformer.rule.RuleExecutor.executeRule;
import static com.peoplespheres.transformer.shared.Param.FULL_NAME;
import static com.peoplespheres.transformer.util.EmailUtils.filterIllegalCharacters;
import static com.peoplespheres.transformer.util.ParamUtils.getParamValue;

@Service
@RequiredArgsConstructor
public class GenerateEmailsImpl implements GenerateEmails {

	private final TransformerValidator validator;

	private static final Logger logger = LoggerFactory.getLogger(GenerateEmailsImpl.class);
	private static final String WHITE_SPACES = "\\s+";

	@Override
	public Emails generate(final EmailExpression expression, final LinkedMultiValueMap<String, String> requestParams) {
		final var arguments = extractArgumentsFrom(expression);
		final var emails = new ArrayList<Email>();

		extractEmailsParamsFrom(requestParams, arguments).forEach(emailParams -> emails.add(newEmailFrom(emailParams, expression)));

		logger.info("Successfully generated email for {} users", emails.size());

		return new Emails(emails);
	}

	private Email newEmailFrom(final Map<String, String> emailParams, final EmailExpression expression) {
		final var builder = new StringBuilder();
		final var nameConcatenation = getNameConcatenation(expression.name());
		final var domainConcatenation = ".";
		final var name = processExpression(expression.name().elements(), emailParams, nameConcatenation);
		final var domain = processExpression(expression.domain().elements(), emailParams, domainConcatenation);
		final var product = builder.append(name).append("@").append(domain).toString().toLowerCase();
		final var filtered = filterIllegalCharacters(product);
		validator.validateEmail(filtered);
		return new Email(filtered, filtered);
	}

	private List<Map<String, String>> extractEmailsParamsFrom(
			final LinkedMultiValueMap<String, String> requestParams,
			final List<String> expressionArguments
	) {
		final var emailsCount = expressionArguments.stream()
				.mapToInt(target -> Math.max(requestParams.getOrDefault(target, List.of()).size(), 1))
				.max()
				.orElse(1);

		final var emailsParams = new ArrayList<Map<String, String>>();

		for (int mapIndex = 0; mapIndex < emailsCount; mapIndex++) {
			final var emailParams = new LinkedHashMap<String, String>();

			for (String argument : expressionArguments) {
				final var values = requestParams.getOrDefault(argument, List.of(""));
				final var value = values.size() > mapIndex ? values.get(mapIndex) : values.getFirst();
				emailParams.put(argument, value);
			}

			emailsParams.add(emailParams);
		}

		return emailsParams;
	}

	private String processExpression(
			final List<ComponentElement> expressionElements,
			final Map<String, String> emailParams,
			final String concatenation
	) {
		final var builder = new StringBuilder();

		expressionElements.stream()
				.filter(ExpressionUtils::isNotConcatenationRule)
				.forEach(expressionElement -> {
					final var value = getParamValue(expressionElement.target(), emailParams);

					if (expressionElement.target().equals(FULL_NAME.getName())) {
						Arrays.stream(value.split(WHITE_SPACES)).forEach(name -> {
									final var transformed = executeRule(expressionElement.rule(), name);
									builder.append(transformed);
									builder.append(concatenation);
								}
						);
					} else {
						final var transformed = executeRule(expressionElement.rule(), value);
						builder.append(transformed);
						builder.append(concatenation);
					}
				});

		if (builder.toString().endsWith(concatenation)) {
			builder.setLength(builder.length() - 1);
		}

		return builder.toString();
	}
}
