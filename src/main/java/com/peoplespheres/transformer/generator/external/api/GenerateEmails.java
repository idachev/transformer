package com.peoplespheres.transformer.generator.external.api;

import com.peoplespheres.transformer.expression.EmailExpression;
import com.peoplespheres.transformer.model.Emails;
import org.springframework.modulith.NamedInterface;
import org.springframework.util.LinkedMultiValueMap;

@NamedInterface
@FunctionalInterface
public interface GenerateEmails {
	Emails generate(final EmailExpression expression, final LinkedMultiValueMap<String, String> params);
}
