package com.peoplespheres.transformer.api.internal.config;

import com.peoplespheres.transformer.api.internal.handler.EmailGeneratorHandler;
import com.peoplespheres.transformer.model.Emails;
import com.peoplespheres.transformer.model.ErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.RouterOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
@RequiredArgsConstructor
public class RoutesConfig {

	private final ApiProperties api;
	private final EmailGeneratorHandler emailGeneratorHandler;

	@RouterOperation(
			method = GET,
			path = "/v1/transformer/generate/email",
			produces = {APPLICATION_JSON_VALUE},
			beanClass = EmailGeneratorHandler.class,
			beanMethod = "generateEmail",
			operation = @Operation(
					operationId = "generateEmail",
					tags = {"Email Generation"},
					parameters = {
							@Parameter(
									name = "firstName", description = "First Name of the user's email",
									examples = {@ExampleObject(value = "john")}
							),
							@Parameter(
									name = "middleName", description = "Middle Name of the user's email",
									examples = {@ExampleObject(value = "doe")}
							),
							@Parameter(
									name = "lastName", description = "Last Name of the user's email",
									examples = {@ExampleObject(value = "smith")}
							),
							@Parameter(
									name = "fullName", description = "Full Name of the user's email: 'Ivo Georgiev Georgiev'",
									examples = {@ExampleObject(value = "John Doe Smith")}
							),
							@Parameter(
									name = "role", description = "The role of the user: 'dev', 'hr', 'devops'",
									examples = {@ExampleObject(value = "dev")}
							),
							@Parameter(
									name = "department", description = "The department of the user: 'Human Resources', 'Backend', 'Frontend'",
									examples = {@ExampleObject(value = "backend")}
							),
							@Parameter(
									name = "resourceType", description = "The resource type: 'internal', 'external'",
									examples = {@ExampleObject(value = "internal")}
							),
							@Parameter(
									name = "entity", description = "The entity in which the user works: 'peoplespheres', 'google', 'cordona'",
									examples = {@ExampleObject(value = "peoplespheres")}
							),
							@Parameter(
									name = "domain", description = "The entity's domain: 'com', 'fr', 'bg'",
									examples = {@ExampleObject(value = "com")}
							),
							@Parameter(
									name = "entityDomain", description = "A combination of entity and domain: 'peoplespheres.com', 'google.com'",
									examples = {@ExampleObject(value = "peoplespheres.com")}
							),
							@Parameter(
									name = "expression", description = "The expression used for email generation. Must be a Base 64 encoded string.",
									required = true,
									examples = {@ExampleObject(value = "cHNkZWxzK3VzZSthbGw=")}
							)
					},
					responses = {
							@ApiResponse(
									responseCode = "200",
									description = "Successful operation",
									content = @Content(schema = @Schema(implementation = Emails.class))
							),
							@ApiResponse(
									responseCode = "400",
									description = "Bad request operation",
									content = @Content(schema = @Schema(implementation = ErrorResponse.class))
							),
							@ApiResponse(
									responseCode = "500",
									description = "Internal server error operation",
									content = @Content(schema = @Schema(implementation = ErrorResponse.class))
							)
					}
			)
	)
	@Bean
	public RouterFunction<ServerResponse> generateEmail() {
		final var generateUriProps = api.getUri().getGenerate();
		return route(
				GET(generateUriProps.getBase() + generateUriProps.getEmail()),
				emailGeneratorHandler::generateEmail
		);
	}
}
