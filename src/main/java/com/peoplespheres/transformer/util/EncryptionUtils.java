package com.peoplespheres.transformer.util;


import java.util.Base64;

public class EncryptionUtils {

	private EncryptionUtils() {
	}

	public static String decodeFromBase64(final String encoded) {
		return new String(Base64.getDecoder().decode(encoded));
	}

	public static String obfuscate(String target) {
		final var obfuscated = new StringBuilder(target.length());
		for (int i = 0; i < target.length(); i++) {
			if (target.charAt(i) == '@') {
				obfuscated.append(target.substring(i));
				break;
			}
			if (i % 2 == 1) {
				obfuscated.append('*');
			} else {
				obfuscated.append(target.charAt(i));
			}
		}
		return obfuscated.toString();
	}
}
