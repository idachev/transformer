package com.peoplespheres.transformer.shared;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

@Getter
public enum Rule {
	USE_ALL("useAll"),
	USE_FIRST("useFirst"),
	USE_LAST("useLast"),
	EVAL("eval"),
	CONCATENATE_WITH("concatenateWith");

	private final String name;

	Rule(String name) {
		this.name = name;
	}

	public static Optional<Rule> fromName(String name) {
		return Arrays
				.stream(Rule.values())
				.filter(rule -> rule.name.equals(name))
				.findFirst();
	}
}