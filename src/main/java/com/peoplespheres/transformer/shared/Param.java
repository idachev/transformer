package com.peoplespheres.transformer.shared;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

@Getter
public enum Param {
	FIRST_NAME("firstName"),
	MIDDLE_NAME("middleName"),
	LAST_NAME("lastName"),
	FULL_NAME("fullName"),
	ROLE("role"),
	DEPARTMENT("department"),
	RESOURCE_TYPE("resourceType"),
	DOMAIN("domain"),
	ENTITY("entity"),
	ENTITY_DOMAIN("entityDomain"),
	EXPRESSION("expression");

	private final String name;

	Param(String name) {
		this.name = name;
	}

	public static Optional<Param> fromName(String name) {
		return Arrays
				.stream(Param.values())
				.filter(param -> param.getName().equals(name))
				.findFirst();
	}

}