package com.peoplespheres.transformer.exception;

public final class GenericException extends TransformerException {

	public GenericException(ExceptionDetails details) {
		super(details);
	}
}
