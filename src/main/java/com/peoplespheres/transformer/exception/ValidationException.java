package com.peoplespheres.transformer.exception;

public final class ValidationException extends TransformerException {

	public ValidationException(ExceptionDetails details) {
		super(details);
	}
}
