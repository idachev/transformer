package com.peoplespheres.transformer.exception;

public final class ExpressionException extends TransformerException {

	public ExpressionException(ExceptionDetails details) {
		super(details);
	}
}
