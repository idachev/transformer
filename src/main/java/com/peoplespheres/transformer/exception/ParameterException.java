package com.peoplespheres.transformer.exception;

public final class ParameterException extends TransformerException {

	public ParameterException(ExceptionDetails details) {
		super(details);
	}
}
