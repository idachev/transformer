package com.peoplespheres.transformer.expression;

public record ComponentElement(String target, String rule) {
}
