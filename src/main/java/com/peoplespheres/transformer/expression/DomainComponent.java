package com.peoplespheres.transformer.expression;

import java.util.List;

public record DomainComponent(List<ComponentElement> elements) {
}
