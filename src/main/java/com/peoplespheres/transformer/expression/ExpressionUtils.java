package com.peoplespheres.transformer.expression;

import com.peoplespheres.transformer.exception.ExpressionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static com.peoplespheres.transformer.shared.Rule.CONCATENATE_WITH;
import static com.peoplespheres.transformer.util.ExceptionUtils.logAndRaise;

public class ExpressionUtils {

	private ExpressionUtils() {
	}

	private static final Logger logger = LoggerFactory.getLogger(ExpressionUtils.class);

	private static final String CONCATENATION_RULE = CONCATENATE_WITH.getName();
	private static final Set<Character> ALLOWED_CONCATENATION_CHARACTERS = Set.of('.', '_', '-');

	public static String getNameConcatenation(final NameComponent component) {
		return component.elements()
				.stream()
				.filter(e -> e.target().equals(CONCATENATION_RULE))
				.map(ComponentElement::rule)
				.findFirst()
				.map(character -> {
					if (isNotAllowedConcatenation(character)) {
						throw logAndRaise(
								logger,
								String.format("Illegal name concatenation character: '%s'. Allowed characters are '_','.','-'.", character),
								ExpressionException.class);
					}
					return character;
				})
				.orElseThrow(() ->
						logAndRaise(
								logger,
								String.format("Expression does not contain rule '%s'", CONCATENATION_RULE),
								ExpressionException.class
						)
				);
	}

	public static List<String> extractArgumentsFrom(final EmailExpression expression) {
		return Stream.concat(
						expression.name().elements().stream(),
						expression.domain().elements().stream()
				)
				.filter(ExpressionUtils::isNotConcatenationRule)
				.map(ComponentElement::target)
				.toList();
	}

	public static boolean isNotConcatenationRule(final ComponentElement element) {
		return !element.target().equals(CONCATENATION_RULE);
	}

	private static boolean isNotAllowedConcatenation(final String character) {
		return character.isEmpty() || !ALLOWED_CONCATENATION_CHARACTERS.contains(character.charAt(0));
	}

}
