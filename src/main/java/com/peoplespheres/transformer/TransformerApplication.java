package com.peoplespheres.transformer;

import org.springframework.boot.SpringApplication;
import org.springframework.modulith.Modulith;

@Modulith
public class TransformerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransformerApplication.class, args);
	}

}
