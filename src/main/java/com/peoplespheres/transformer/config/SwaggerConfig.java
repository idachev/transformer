package com.peoplespheres.transformer.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class SwaggerConfig {

	public static final String TITLE = "Email Transformer Service";
	public static final String DESCRIPTION = "Email Transformer service that implements PSDSEL (PeopleSpheres Domain Specific Expression Language)";
	public static final String VERSION = "0.1.0";
	public static final String TERMS_OF_SERVICE_URL = "https://peoplespheres.com/resources/legal-stuff/";
	public static final String LICENSE = "Apache License Version 2.0";
	public static final String LICENSE_URL = "https://www.apache.org/licenses/LICENSE-2.0";
	public static final String CONTACT_NAME = "Ivo Georgiev";
	public static final String CONTACT_URL = "https://info.peoplespheres.com/en-us/contact";
	public static final String CONTACT_EMAIL = "igeorgiev@peoplespheres.com";

	@Bean
	public GroupedOpenApi publicApi() {
		return GroupedOpenApi.builder()
				.group("transformer")
				.packagesToScan("com.peoplespheres.transformer")
				.pathsToMatch("/**")
				.build();
	}

	@Bean
	public OpenAPI customOpenAPI() {
		return new OpenAPI()
				.info(new Info()
						.title(TITLE)
						.description(DESCRIPTION)
						.version(VERSION)
						.termsOfService(TERMS_OF_SERVICE_URL)
						.license(new License().name(LICENSE).url(LICENSE_URL))
						.contact(new Contact()
								.name(CONTACT_NAME)
								.url(CONTACT_URL)
								.email(CONTACT_EMAIL))
				);
	}
}
